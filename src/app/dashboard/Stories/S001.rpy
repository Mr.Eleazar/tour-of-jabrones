init python:
    global numdays
    global EndGame
    EndGame = False
    numdays = 0

label classmenu:
scene BG_HallClassroom1
menu:
    "Skip classes.":
        jump specialmenu
    "Go to Art Class.":
        $artmain(numdays)
    "Go the Gym Class.":
        $gymmain(numdays)
    "Go to History Class.":
        $historymain(numdays)
    "Go to Tutoring Service.":
        $tutormain(numdays)
    "Go to Station Service.":
        $stationmain(numdays)
    "Go to Admin Service.":
        $adminmain(numdays)

label specialmenu:
scene BG_BedroomMC
menu:
    "There's nothing important to do.":
        jump daychange
    "Talk to Btier" if Art_Morning_Btier_Request_Available:
        jump Aktrenat_Btiers_Help
    "Paint Some Fruit" if Art_Night_Painting_Available:
        jump Night_Painting_Fruit
    "Return the Books" if Art_Night_Bracelet_Scene_Available:
        jump Art_Night_Bracelet_Scene
    "Chose Your Model" if Art_Model_Selection_Scene_Available:
        jump Aktrenat_Model_Selection
    "Read the Materials" if Art_Night_Reading_Scene_Available:
        jump Aktrenat_Night_Reading
    "Paint Your Model" if Art_Night_Model_Painting_Scene_Available:
        jump Art_Night_Model_Painting

label daychange:
$numdays += 1
jump classmenu

label splashscreen:
    scene Banner_InGame with fade
    $ renpy.pause(6.0, hard=True)
    return

label start:
window hide

#SKYLINE
scene SC_Intro1 with fade

"Congratulations, Cadet Jacinto on your acceptance into the Federal Academy System and to your future career in the Federal Service."
"Your stellar academic and extracurricular career since your last rejection along with other directives,
has caused the committee to reconsider your application."
"Your free college education in the Federal Academy System must be repaid with a length of time in the Federal Service."
"This length depends on the needs of the Federation, with your placement being determined by the needs of the Federation,
your aptitudes, and your performance."
"Afterword, your career in the Federation can be continued in your current path, you may request a transfer that requires approval,
or you can retire."
"Failure to follow the rules or meet the requirements of the Federal Academy System is grounds for expulsion with back payment
for all education rendered."
"Enclosed is directives to memorize, the details of your assigned academy, and booking to arrive at it as soon as possible."
"We wish you well with your future academic and career pursuits, for Liberty, Tranquility, and Prosperity."
"Federal Ministry of Education, Federal Academy Bureau, Circasol Region"

# MOM HUG 1
scene SC_Intro2 with fade
"I know there’s not much for me here on Earth, but I’m still going to miss the charms of this burnt over and out rock."

# MOM HUG 2
scene SC_Intro3 with fade
"While I am sad to leave but they’re both acting like I am going away to maximum security prison."

# MC WAVING
scene SC_Intro4 with fade
"Well, I could be if that’s the job I get if I don’t get to be a naval or science officer."

# MOMS GOOD BYE
scene SC_Intro5 with fade
"They act like there isn’t an interstellar superpower who needs reliable communications to function."

#LIFT OVER
scene SC_Intro6 with fade
"I’ll send them a message after I dock with the liner and send them messages as much as possible...that should make them less worried."

#MC IN SHUTTLE (NOT NEW)
scene SC_Intro7 with fade
"Been launched a while ago but it shouldn’t be long until I get visual confirmation."

#SHUTTLE VIEW OF THE STATION (NOT NEW)
scene SC_Intro8 with fade
"And there she is, Zero G Academy, the ticket that got me to the stars."

###TOUR
scene BG_Lobby with fade
show ForaUniformAngryLeft at left
Unknown "Why do I always get the yokel new students and not anyone more cosmopolitan?"
hide ForaUniformAngryLeft
show MCCasualNormalRight at right
show ForaUniformTourLeft at left
Unknown "Oh...um..Hello...Cadet Jacinto and welcome to the academy."
Unknown "I’m Fora and I am a hall monitor who's going to be your tour guide today."
Fora "What's your first name by the way?"
hide ForaUniformTourLeft
hide MCCasualNormalRight

jump InputPlayerConfig
label ThePlayerName:

show screen text_input_screen()
"Click on the textbox and type your firstname!"
#MC "Check the checkbox button when your done!."
hide screen text_input_screen
#$ PlayerName = renpy.input ("Enter your name... ")
#$ PlayerName = PlayerName.strip()
#if PlayerName == "":
#    $PlayerName = "MC"
scene BG_Lobby
if PlayerName == "":
    $PlayerName = "MC"
"Is your firstname, [PlayerName]?"
menu:
        "Yeah, I find that not objectionable.":
            jump AfterNameInput
        "No, who would want to be called that...":
            jump InputPlayerConfig

label AfterNameInput:
scene BG_Lobby

show MCCasualHappyRight at right
show ForaUniformTourLeft at left
MC "It’s [PlayerName] and I am happy to be here."
Fora "All right  [PlayerName], let’s give you the tour."
hide MCCasualHappyRight
hide ForaUniformTourLeft

#ART
scene BG_ArtClassroom with fade

show ForaUniformTourRight at right
show MCCasualNormalLeft at left
Fora "Here’s our state of the art...Art Classroom?"
Fora "The teacher here is Ms.Aktrenat she’s one of the best artists of her generation and a master of many styles."
Fora "She’s nurturing and pleasant teacher who enjoys her role of mentoring the next generation of creatives."
hide ForaUniformTourRight
hide MCCasualNormalLeft

#GYM
scene BG_Gym with fade

show ForaUniformTourRight at right
show MCCasualNormalLeft at left
Fora "And here is the Athletic Wing which is home to numerous facilities for physical education."
Fora "But more importantly it’s home to our champion Brutal Ball team led by Coach Yulvar whose by extension a champion."
hide MCCasualNormalLeft
show MCCasualScaredLeft at left
Fora "She’s also our athletics director and had quite the career in the sport until her expulsion due to repeated chainsaw related infractions..."
hide ForaUniformTourRight
hide MCCasualScaredLeft

#HISTORY
scene BG_Classroom with fade

show ForaUniformTourRight at right
show MCCasualNormalLeft at left
Fora "And this one of our standard lecture halls with a set up that allows and promotes interactivity during lectures."
Fora "This hall is for a civilizational course taught by our newest teacher, Anet Parray who specializes in human studies."
Fora "From some rumors, she’s quite the iconoclast and expects vigorous students up for intense debate."
hide ForaUniformTourRight
hide MCCasualNormalLeft

#MAINTENANCE  (SKIPPED)
scene Placeholder with fade

show ForaUniformTourRight at right
show MCCasualNormalLeft at left
Fora "And this room is where the Station’s operations to function and supply services are run from."
Fora "Any and all issues related to facility are resolved here under the watchful eyes of Chief Station Officer Gaske Mara and Station’s AI, VEX."
Fora "If you have complaints or concerns submitting a ticket to VEX and things will clear things up shortly."
hide ForaUniformTourRight
hide MCCasualNormalLeft

#TUTORING (SKIPPED)
scene Placeholder with fade

show ForaUniformTourRight at right
show MCCasualNormalLeft at left
Fora "The Tutoring Center is for students needing help in passing their classes."
Fora "The tutors are students who receive credits for their expertise."
Fora "Which helps keep the more troubled students from being flunked out and the academy from losing funding…"
hide ForaUniformTourRight
hide MCCasualNormalLeft

#YOUR ROOM
scene BG_BedroomMC with fade

show ForaUniformTourRight at right
show MCCasualNormalLeft at left
Fora "And this is a single bedroom, one of the few such available for students."
Fora "Given your partial completion of college education among other factors you were awarded it."
Fora "It’s located very close to the center of station, right next to the main elevators."
hide ForaUniformTourRight
hide MCCasualNormalLeft

#OFFICE

scene BG_PrincipalOffice with fade

show ForaUniformTourRight at right
show MCCasualNormalLeft at left
Fora "And this is where the Academy itself is run from."
Fora "Here are the offices and work areas for numerous faculty."
Fora "And as this is the end of our tour, please go meet Principal Libya."
Fora "She’s wanting to assign your class and give your credentials."
hide ForaUniformTourRight
hide MCCasualNormalLeft

# 1
scene SC_Libya_Oyster1 with fade
MC "Hello?"
Unknown "Come in…"

#2
scene SC_Libya_Oyster2 with fade
Unknown "Welcome to my academy, I am your principal Libya Voada."
MC "I’m [PlayerName], it’s a pleasure to meet you."

#3
scene SC_Libya_Oyster3 with fade
Libya "As you can tell….I am in the middle of my lunch…"

MC "I can wait outside…"

Libya "Nonsense, I will be done in a minute."

#4
scene SC_Libya_Oyster4
"Cutting Noises."

#5
scene SC_Libya_Oyster5 with vpunch
"Plinking Noises."

# 6
scene SC_Libya_Oyster6 with hpunch
"Sucking Noises."

# 7

scene SC_Libya_Oyster7 with vpunch
"Plinking Noises."
"Cutting Noises."
scene SC_Libya_Oyster7 with vpunch
"Plinking Noises."
"Sucking Noises."

# 8

# Do a transition to this by a jump and then keep jumping to the image.
MC "{i}She's really going at it now...{/i}."

scene SC_Libya_Oyster8 with vpunch
"Plinking Noises."
"Cutting Noises."

scene SC_Libya_Oyster8 with vpunch
"Plinking Noises."
"Sucking Noises."

scene SC_Libya_Oyster8 with vpunch
"Plinking Noises."
"Cutting Noises."

scene SC_Libya_Oyster8 with vpunch
"Plinking Noises."
"Sucking Noises."

# 9

scene SC_Libya_Oyster9 with hpunch
Libya "Ahh...Once again, Xenobiology has shown me why they deserved that grant money."

# 10

scene SC_Libya_Oyster10 with vpunch
"Plinking Noises."

scene SC_Libya_Oyster11 with hpunch
Libya "A pity those snails aren’t ready yet..."

# NORMAL CONVO
scene BG_PrincipalOffice

show MCCasualScaredLeft at left
show LibyaUniformNormalRight at right
Libya "Now that I am sated..."
Libya "It’s time to size up the latest late inclusivity admission to saunter in."
hide MCCasualScaredLeft
show MCCasualWorriedLeft at left
hide LibyaUniformNormalRight
show LibyaUniformSarcasticRight at right
MC "Inclusivity admission?"
Libya "Special acceptance for stability and to reach under tapped sources of manpower for the Federal Service."
Libya "As you are from...Earth."
Libya "The most neglected of any human world that is currently controlled by the Federation in accordance with the Treaty of Crux Ultima."
MC "Which is why I was accepted?"
Libya "Yes, there’s quite an elaborate ranking and system."
Libya "And through multiple factors namely your academic achievements and being from a rough world with no cadets so far..."
hide MCCasualWorriedLeft
show MCCasualSmugLeft at left
MC "Edged me to the top of the bracket?"
Libya "The human bracket which has around 400 slots opening up a year.."
MC "So what classes am I going to take since I didn’t get to pick any."
hide LibyaUniformSarcasticRight
show LibyaUniformNormalRight at right
Libya "I like to offer you a deal..we’re a bit short staffed in regard to not having enough students take up supporting roles."
Libya "If you would offer to volunteer your time to work these roles..I will be in your debt."
hide MCCasualSmugLeft
show MCCasualSarcasticLeft at left
MC "I assume that they count as-"
hide LibyaUniformNormalRight
show LibyaUniformSarcasticRight at right
Libya "As credit, of course they do."
Libya "I need three positions filled and your workload is within class time so don’t be worried about that."
hide MCCasualSarcasticLeft
show MCCasualWorriedLeft at left
MC "Three...that’s quite a lot."
Libya "Well I am generous to those who help and show merit...I give rewards such as your very own room."
MC "And I assume you will-"
hide LibyaUniformSarcasticRight
show LibyaUniformHappyRight at right
Libya "Oh no, it’s too late to make you bunk with the savages."
Libya "Besides, I don’t use threats...I use gentle persuasion."
Libya "If you help me, I will provide strong recommendation since you will be directly working with me for one of these jobs."
Libya "The other two will be worthy additions that show you are quite...skilled."
hide MCCasualWorriedLeft
show MCCasualNormalLeft at left
MC "Alright, and I assume my actual classes will be-"
hide LibyaUniformHappyRight
show LibyaUniformSarcasticRight at right
Libya "As easy as possible..you’ve already been to every location you really need to be…"
MC "So I am taking-"
Libya "Human Civilization 1945-3XXX taught by Anet Parray, Art Class I taught by Ms. Aktrenat, Mandated Athletics supervised by Coach Yulvar will be your actual courses."
Libya "And you will serve the Academy through Administrative Service with yours truly where you will be my assistant in the morning."
Libya "And in Tutoring Service by ensuring our test scores and failures are within tolerable levels where I am sure you will make a difference."
Libya "And in Station Service where you will be a technician underneath Gaske and VEX who are professionally competent and personally incompetent."
Libya "Sound fine?"
hide LibyaUniformSarcasticRight
show LibyaUniformNormalRight at right
hide MCCasualNormalLeft
show MCCasualSarcasticLeft at left
MC "It’s nothing I haven’t done before or can’t learn on the job."
hide LibyaUniformNormalRight
show LibyaUniformHappyRight at right
Libya "And that’s how I like my cadets, capable and willing to press forward."
hide MCCasualSarcasticLeft
show MCCasualHappyLeft at left
Libya "And lastly before I dismiss you, here are your credentials, your papers, a map, your schedule, and more."
Libya "I will see you in the morning and make sure you sleep to get adjusted to the schedule."
MC "Thanks, Principal Libya."
hide MCCasualHappyLeft
hide LibyaUniformHappyRight

jump daychange
