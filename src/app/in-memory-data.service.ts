import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Suddenly Gay For No Reason' },
      { id: 12, name: 'Suddenly Not Gay For No Reason' },
      { id: 13, name: 'Twink' },
      { id: 14, name: 'Buttgirl Redux' },
      { id: 15, name: 'Ass And Has Ass' },
      { id: 16, name: 'Mandated POC Character' },
      { id: 17, name: '1/2 Of My Favorite Porn Genre' },
      { id: 18, name: '1/2 Of My Favorite Porn Genre' },
      { id: 19, name: 'Virgin Medic' },
      { id: 20, name: 'Holocaust Class' },
      { id: 21, name: 'Competent Gorilla American' },
      { id: 22, name: 'Future Pakistani Rape Slave' }
    ];
    return {heroes};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}
